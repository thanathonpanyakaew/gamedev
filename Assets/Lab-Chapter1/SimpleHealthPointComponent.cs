using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleHealthPointComponent : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] public const float MAX_HP = 100;
    
    
    [SerializeField] 
    private float m_HealthPoint;
    
    //Property
    public float HealthPoint
    {
        get
        {
            return m_HealthPoint;
            
        }
        set
        {
            m_HealthPoint = value;
            
            if (value > 0)
            {
                if (value <= MAX_HP)
                {
                    m_HealthPoint = value;
                    if (HealthPoint > MAX_HP)
                    {
                        m_HealthPoint = MAX_HP;
                    }
                }
                else
                {
                    m_HealthPoint = MAX_HP;
                }
            }
            
        }
    }
    
   
    // Update is called once per frame
    void Update()
    {
        //HealthPoint += 1;
    }
}
