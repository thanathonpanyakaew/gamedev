using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aby.GameDev3.Chapter5.InteractionSystem
{
    public interface IInteractable : IEnumerable
    {
        void Interact(GameObject actor);
        
    }
}

