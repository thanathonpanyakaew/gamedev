using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameDev3/Util/ScriptableObjectUtils")]
public class ScriptableObjectUtils : ScriptableObject
{
	public void Destroy(GameObject gameObject)
	{
		Object.Destroy(gameObject);
	}
}
