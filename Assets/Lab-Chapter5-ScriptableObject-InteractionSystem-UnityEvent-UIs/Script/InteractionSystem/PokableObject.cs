using System.Collections;
using TMPro;
using UnityEngine;

namespace Aby.GameDev3.Chapter5.InteractionSystem
{
    public class PokableObject : MonoBehaviour, IInteractable, IActorEnterExitHandler
    {
        [SerializeField] protected TextMeshProUGUI m_InteractionTxt;
        [SerializeField] protected float m_Power = 10;
        private Rigidbody m_rigidBody;

        private void Start()
        {
            m_rigidBody = GetComponent<Rigidbody>();
        }

        public void Interact(GameObject actor)
        {
	        m_rigidBody.AddForce(Vector3.up * m_Power, ForceMode.Impulse);
        }

        public void Interact()
        {
            m_rigidBody.AddForce(Vector3.up * m_Power, ForceMode.Impulse);
        }

        public void ActorEnter()
        {
            m_InteractionTxt.gameObject.SetActive(true);
        }

        public void ActorExit()
        {
            m_InteractionTxt.gameObject.SetActive(false);
        }

        public IEnumerator GetEnumerator()
        {
            throw new System.NotImplementedException();
        }

        public void ActorEnter(GameObject actor)
        {
	        m_InteractionTxt.gameObject.SetActive(true);
        }

        public void ActorExit(GameObject actor)
        {
	        m_InteractionTxt.gameObject.SetActive(false);
        }
    }

}

