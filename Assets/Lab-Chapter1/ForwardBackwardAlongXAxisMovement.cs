using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForwardBackwardAlongXAxisMovement : MonoBehaviour
{
    // Start is called before the first frame update
    
    public const float MAX_MOVEMENT_DISTANCE = 2.0f; //กำหนดค่าคงที่
    
    private float m_DisplacementCounter = 0; //
    
    [SerializeField] 
    private float m_XComponentSpeed = 0.02f;// สร้างตัวแปรเก็บค่าความเร็วในแกน X (SerializeField จะโชว์ตัวแปรใน inspector)
    
    private Vector3 m_MovementSpeed = Vector3.zero; // สร้างตัวแปรเก็บการเคลือนที่ 3 มิติ
    
        void Start()
        {
            m_MovementSpeed.x = m_XComponentSpeed; //กำหนดให้ตัวแปรการเคลือนที่ในแกน x = 0.02
        }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += m_MovementSpeed;

        m_DisplacementCounter += m_MovementSpeed.x;

        if (MathF.Abs(m_DisplacementCounter) > MAX_MOVEMENT_DISTANCE)
        {
            m_DisplacementCounter = 0;
            m_MovementSpeed *= -1;
        }
    }
}
